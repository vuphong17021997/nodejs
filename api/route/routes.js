module.exports = function(app){
    var todolist = require('../controller/controller')
    app.route('/tasks')
    .get(todolist.get_all_tasks)
    .post(todolist.create_a_task);
    app.route('/task/:taskId')
    .put(todolist.update_a_task)
    .delete(todolist.delete_a_task);
    app.route('/update_position')
    .put(todolist.update_position_task)
    
}