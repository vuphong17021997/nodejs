var monggoose = require('mongoose');
var Schema = monggoose.Schema;

var TaskSchema = new Schema({
    name:{
        type:String,
        required:[true,'Kindly enter the name of the task']
    },
    created_date:{
        type:Date,
        default:Date.now
    },
    status:{
        type:[{
            type:Boolean,
            enum:[false,true]
        }],
        default:[false]
    },
    order:{
        type:Number,
        required:[true,"Kindly enter position"]
        
    }
});
module.exports = monggoose.model('task',TaskSchema);
