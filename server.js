// const { query } = require("express");

var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require("body-parser");
task = require ('./api/model/model')

//thiet lap connection
mongoose.connect('mongodb://localhost/task', {
    useNewUrlParser:true,
    useCreateIndex:true,
    useUnifiedTopology: true,
})

//thiet lap phan tich cac request toi serer
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

// truy cap toi cac tep tin trong route
var route = require('./api/route/routes')
route(app);

//lang nghe cacs ket noi tren may chu ca cong duoc chi dinh
app.listen(3001, ()=>{
    console.log("connected to port 3001")
})